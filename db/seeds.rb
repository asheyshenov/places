require 'faker'

20.times do |r|
  User.create(
      name: Faker::Name.name,
      email: Faker::Internet.email,
      password: "321321")
end

20.times do |r|
  Category.create(
   name: Faker::Name.name
  )
end

User.all.each do |user|
  for i in 1..13
    place = user.places.build(title: Faker::Book.title, description: Faker::Lorem.sentence, category_id: "#{rand(1..20)}")
    path = Rails.root.join('app', 'assets', 'images', 'fixtures', "#{i}.jpg")
    place.picture.attach(io: File.open(path), filename: "#{i}.jpg")
    place.save
    if i > 30
      break
    end
  end
end