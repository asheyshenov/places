Rails.application.routes.draw do
  get 'reviews/create'
  devise_for :users
  root "places#index"
  resources :places do
    resources :reviews
  end
  resources :categories
  resources :users, :only => [:index, :show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
