class Place < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_one_attached :picture
  has_many :reviews
  self.per_page = 20

end
