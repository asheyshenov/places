class Ability
  include CanCan::Ability
  def initialize(user)
    can :read, Place
    can :read, Category

    if user.present?
      can :create, Place
      can :edit, Place, user_id: user.id
      can :destroy, Place, user_id: user.id
      if user.admin?
        can :manage, :all
      end
    end
  end
end