class PlacesController < ApplicationController
  load_and_authorize_resource
  def index
    @places  = Place.paginate(page: params[:page])
  end

  def new
    @place = Place.new()
  end

  def show
    @place = Place.find(params[:id])
  end

  def create
    @place = current_user.places.build(place_params)
    if @place.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
    @place = Place.find(params[:id])
  end

  def upload_picture
    @place.picture.attach(uploaded_file) if uploaded_file.present?
  end

  def uploaded_file
    params[:place][:picture]
  end

  def update
    @place = Place.find(params[:id])
    if @place.update(place_params)
      redirect_to @place
    else
      render 'edit'
    end
  end


  def destroy
    @place = Place.find(params[:id])
    @place.destroy
    redirect_to root_path
  end

  private

  def place_params
    params.require(:place).permit(:title, :description, :category_id, :picture, :search)
  end
end
